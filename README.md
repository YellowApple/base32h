# Base32H

Base32H is a variant of a base-32 numeral system designed specifically
to maximize human readability.  It's a variation on [Crockford's
Base32](http://crockford.com/wrmg/base32.html), with the addition of
`U`/`u` (as an alias on `V`, thus at least partially preserving
Crockford's mild defense against accidental profanity), the separation
of uppercase `L` from lowercase `l` and `1`/`i`/`I` (`L` is pretty
visually distinct), and the conversion of `S`/`s` into an alias for
`5`.  The key advantage here is that any alphanumeric text can be
represented by this particular base-32 digit set; this is not really
the case for Crockford's (which treats `U` as a check digit) or RFC
4648 (which can't make sense of a number containing the digits `8` or
`9`).  It also means that any readability ambiguities between `5` and
`S` are eliminated (neither Crockford's nor RFC 4648 address this at
all).

Base32H does not explicitly establish a check digit, but there's
nothing stopping an application from interpreting some of the
Crockford check digits (`*`, `-`, `$`, or `=`).  `U` is no longer
usable as a check digit, but there are certainly alternative symbols
available (the author recommends `.` for maximum readability and
minimal interference with URLs per RFC 3968).

Base32H is ideal for situations where a number needs to be read aloud
and is recorded using a typeface that does not clearly disambiguate
`0oO`/`1iIl`/`5sS` (the three most common sources of confusion when
reading alphanumeric characters).  It's also a little bit more
space-efficient than base-16 (though not nearly as space efficient as
some other encoding schemes; if you need to encode a lot of binary
data, you're probably better served by base-64).

The intended use case is to create human-readable identifiers that a
machine can treat as a normal integer.  For example, the word `HOWDY`
is equivalent to the decimal integer `17854910` while the integer
`23983478232` encodes to `PARDNER`.  Of course, `HOWDY` and `h0wdy` are
equivalent here.

Each Base32H digit carries 5 bits of data, and 8 digits carry 5 bytes
(40 bits) of data.  This makes Base32H relatively convenient for
encoding binary data (not quite as convenient as base-16 or base-64,
but much more convenient than, say, a non-power-of-two encoding like
base-26/36).  Of course, not all binary data has a length that's a
multiple of 5 bytes, so it's "standard" (in the sense of what the
author recommends) way of handling this is to left-pad the last 5-byte
chunk with zeroes.  This keeps decoding simple.

Included are multiple encoders/decoders for various languages
(WARNING: most are mostly or even entirely untested; use at your own
risk.  Pull requests welcome).

## Alphabet

Value | Encode | Decode
----- | ------ | ---------
0     | 0      | 0 O o
1     | 1      | 1 I i l
2     | 2      | 2
3     | 3      | 3
4     | 4      | 4
5     | 5      | 5 S s
6     | 6      | 6
7     | 7      | 7
8     | 8      | 8
9     | 9      | 9
10    | A      | A a
11    | B      | B b
12    | C      | C c
13    | D      | D d
14    | E      | E e
15    | F      | F f
16    | G      | G g
17    | H      | H h
18    | J      | J j
19    | K      | K k
20    | L      | L
21    | M      | M m
22    | N      | N n
23    | P      | P p
24    | Q      | Q q
25    | R      | R r
26    | T      | T t
27    | V      | V v U u
28    | W      | W w
29    | X      | X x
30    | Y      | Y y
31    | Z      | Z z

## Implementations

### Tcl

- `base32h.tcl`: (included in repo)

## Copying

It's kind of hard to put a license on a number system, so consider
Base32H itself to be public domain.  Specific encoder/decoder
implementations in this repository are all released under the ISC
License (as used by OpenBSD); see `COPYING` for details.
